import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GenericButtonComponent } from './components/generic-button/generic-button.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SwitchComponent } from './components/switch/switch.component';

@NgModule({
    declarations: [
        GenericButtonComponent,
        PageNotFoundComponent,
        SwitchComponent,
    ],
    exports: [
        GenericButtonComponent,
        PageNotFoundComponent,
        SwitchComponent,
    ],
    imports: [ CommonModule ],
})
export class SharedModule {
}
