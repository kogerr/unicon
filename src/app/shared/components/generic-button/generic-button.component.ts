import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-generic-button',
    styleUrls: ['./generic-button.component.scss'],
    template: '<button [class]="colourClass"><ng-content></ng-content></button>',
})
export class GenericButtonComponent {
    @Input() colourClass = '';
}
