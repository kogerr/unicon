import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-switch',
    styleUrls: ['./switch.component.scss'],
    templateUrl: './switch.component.html',
})
export class SwitchComponent {
    @Input() open = false;

    switch(...elements: Array<{ beginElement: () => void }> ): void {
        elements.forEach(e => e.beginElement());
    }
}
