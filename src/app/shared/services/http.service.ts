import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from '../models/message.interface';
import { PlainText } from '../models/plain-text.interface';
import { Position } from '../models/position.interface';
import { SuccessFlag } from '../models/success-flag.interface';
import { TitledText } from '../models/titled-text.interface';

const messagesEndpoint = 'api/messages';
const positionsEndpoint = 'api/positions';
const aboutUsEndpoint = 'api/about-us';
const portfolioSlidersEndpoint = 'api/portfolio-slides';

@Injectable({
    providedIn: 'root',
})
export class HttpService {

    constructor(private http: HttpClient) {
    }

    getAboutUsSlides(): Observable<PlainText[]> {
        return this.http.get<PlainText[]>(aboutUsEndpoint);
    }

    getPortfolioSlides(): Observable<TitledText[]> {
        return this.http.get<TitledText[]>(portfolioSlidersEndpoint);
    }

    sendMessage(message: Message): Observable<SuccessFlag> {
        return this.http.post<SuccessFlag>(messagesEndpoint, message);
    }

    getPositions(): Observable<Position[]> {
        return this.http.get<Position[]>(positionsEndpoint);
    }
}
