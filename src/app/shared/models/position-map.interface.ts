export interface PositionMap {
    type: 'map';
    title: string;
    content: Array<{ left: string, right: string[] }>;
    class: string;
}
