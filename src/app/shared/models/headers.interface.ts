export interface Headers {
    host: string;
    connection: string;
    'upgrade-insecure-requests': string;
    'user-agent': string;
    accept: string;
    'accept-encoding': string;
    'accept-language': string;
}
