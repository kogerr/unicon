export interface PositionText {
    type: 'text';
    content: string;
    class: string;
}
