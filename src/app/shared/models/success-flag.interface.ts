export interface SuccessFlag {
    success: boolean;
    error?: unknown;
}
