export interface Message {
    _id?: string;
    date: number;
    email: string;
    text: string;
    name: string;
    phone?: string;
}
