import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-experience-pie',
    styleUrls: ['./experience-pie.component.scss'],
    template: `<div class="circle"><ng-content></ng-content></div>
    <div class="bullet">• {{ text }}</div>`,
})
export class ExperiencePieComponent {
    @Input() image = '';
    @Input() text = '';
}
