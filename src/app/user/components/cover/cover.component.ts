import { Component } from '@angular/core';
import { CoverView } from '../../models/cover-view';
import { CoverService } from '../../services/cover.service';

@Component({
    selector: 'app-cover',
    styleUrls: ['./cover.component.scss'],
    templateUrl: './cover.component.html',
})
export class CoverComponent {
    image = '';
    text?: string;

    constructor(coverService: CoverService) {
        coverService.change.subscribe(this.setCover);
    }

    private setCover = (coverView: CoverView) => {
        const isMobileView = this.isMobileView();

        this.image = isMobileView && coverView.mobileImage ? coverView.mobileImage : coverView.image;
        this.text = !isMobileView && coverView.text ?  coverView.text : undefined;
    };

    private isMobileView(): boolean {
        return window.innerWidth < 600;
    }
}
