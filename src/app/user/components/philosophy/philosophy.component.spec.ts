import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhilosophyComponent } from './philosophy.component';

describe('PhilosophyComponent', () => {
    let component: PhilosophyComponent;
    let fixture: ComponentFixture<PhilosophyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PhilosophyComponent],
            imports: [ HttpClientModule ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PhilosophyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should have as title 'philosophy'`, () => {
        fixture = TestBed.createComponent(PhilosophyComponent);
        component = fixture.componentInstance;
        expect(component.title).toEqual('philosophy');
    });

    it('should render title in a h1 tag', () => {
        fixture = TestBed.createComponent(PhilosophyComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('philosophy');
    });

});
