import { Component } from '@angular/core';

@Component({
    selector: 'app-philosophy-tile-separator',
    styleUrls: ['./philosophy-tile-separator.component.scss'],
    template: '<div class="line"></div><div class="line"></div>',
})
export class PhilosophyTileSeparatorComponent {
}
