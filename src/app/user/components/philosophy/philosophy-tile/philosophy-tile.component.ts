import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-philosophy-tile',
    styleUrls: ['./philosophy-tile.component.scss'],
    templateUrl: './philosophy-tile.component.html',
})
export class PhilosophyTileComponent {
    @Input() image = '';
    @Input() title = '';
}
