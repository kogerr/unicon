const securityTimeout = 50;
const offset = 100;

export class CircularList<T> {
    center = 0;
    length = 0;
    window: Array<{ element: T, position: number }> = [];

    constructor(private elements: T[]) {
        this.length = elements.length;
        if (elements.length > 0) {
            this.window = [{ element: elements[0], position: 0 }];
        }
    }

    next(): void {
        if (this.elements.length) {
            const nextElement = this.center + 1 === this.elements.length ? this.elements[0] : this.elements[this.center + 1];
            const nextPosition = this.window[this.window.length - 1].position + offset;
            const next = { element: nextElement, position: nextPosition };

            this.window = this.window = [ ...this.window, next ];
            this.center = this.center + 1 === this.elements.length ? 0 : this.center + 1;

            setTimeout(() => {
                this.window.forEach(e => e.position -= offset);
            }, securityTimeout);
        }
    }

    previous(): void {
        if (this.elements.length > 0) {
            const previousElement = this.center === 0 ? this.elements[this.elements.length - 1] : this.elements[this.center - 1];
            const previousPosition = this.window[0].position - offset;
            const previous = { element: previousElement, position: previousPosition };

            this.window = [previous, ...this.window];
            this.center = this.center === 0 ? this.elements.length - 1 : this.center - 1;

            setTimeout(() => {
                this.window.forEach(e => e.position += offset);
            }, securityTimeout);
        }
    }
}
