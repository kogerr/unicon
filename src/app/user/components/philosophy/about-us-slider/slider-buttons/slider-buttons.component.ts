import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-slider-buttons',
    styleUrls: ['./slider-buttons.component.scss'],
    templateUrl: './slider-buttons.component.html',
})
export class SliderButtonsComponent {
    @Output() direction = new EventEmitter<'right' | 'left'>();
}
