import { Component } from '@angular/core';
import { PlainText } from '../../../../../../server/models/plain-text.interface';
import { HttpService } from '../../../../shared/services/http.service';

@Component({
    selector: 'app-about-us-slider',
    styleUrls: ['./about-us-slider.component.scss'],
    templateUrl: './about-us-slider.component.html',
})
export class AboutUsSliderComponent {
    slides = ['red', 'green', 'blue'];
    index = 0;
    slideCount = 0;

    constructor(http: HttpService) {
        http.getAboutUsSlides().subscribe((value: PlainText[]) => {
            this.slides = value.sort((a, b) => a.index - b.index).map(slide => slide.text);
            this.slideCount = this.slides.length;
            this.hack();
        });
    }

    hack() {
        for (let i = 0; i < 3; i++) {
            this.slides = [...this.slides, ...this.slides];
        }
        this.index = this.slideCount * 7;
    }

    getTranslate() {
        return this.index * -100;
    }

    handleClick(event: 'left' | 'right'): void {
        if (event === 'left') {
            if(this.index > 0) {
                this.index -= 1;
            } else {
                this.index = this.slides.length - 1;
            }
        } else if (event === 'right') {
            if (this.index >= this.slides.length - 1) {
                this.slides = [ ...this.slides, ...this.slides ];
                setTimeout(() => this.index += 1, 20);
            } else {
                this.index += 1;
            }
        }
    }
}
