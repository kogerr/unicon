import { Component } from '@angular/core';
import { Message } from '../../../shared/models/message.interface';
import { SuccessFlag } from '../../../shared/models/success-flag.interface';
import { HttpService } from '../../../shared/services/http.service';

@Component({
    selector: 'app-contact',
    styleUrls: ['./contact.component.scss'],
    templateUrl: './contact.component.html',
})
export class ContactComponent {
    invalidAttempt = false;
    successMessage = false;

    constructor(private httpService: HttpService) {
    }

    send(form: HTMLFormElement): void {
        const valid = form.checkValidity();
        if (valid) {
            const name = (form.name as unknown as HTMLInputElement).value;
            const email = form.email.value;
            const phone = form.tel.value;
            const text = form.message.value;
            const date = Date.now();

            const message: Message = { name, email, phone, text, date };

            this.httpService.sendMessage(message).subscribe((value: SuccessFlag) => {
                if (value.success) {
                    this.successAction(form);
                }
            }, (error: unknown) => console.error(error));
        } else {
            this.successMessage = false;
            this.invalidAttempt = true;
        }
    }

    private successAction(form: HTMLFormElement): void {
        this.invalidAttempt = false;
        this.successMessage = true;
        setTimeout(() => this.successMessage = false, 2000);
        form.reset();
    }
}
