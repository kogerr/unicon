import { Component } from '@angular/core';
import { Position } from '../../../shared/models/position.interface';
import { HttpService } from '../../../shared/services/http.service';

@Component({
    selector: 'app-career',
    styleUrls: ['./career.component.scss'],
    templateUrl: './career.component.html',
})
export class CareerComponent {
    error?: unknown;
    positions: Position[] = [];

    constructor(httpService: HttpService) {
        this.getPositions(httpService);
    }

    private getPositions(httpService: HttpService): void {
        const next = (data: Position[]) => this.positions = data;
        const onError = (error: { error?: unknown }) => this.error = error.error || error;

        httpService.getPositions().subscribe(next, onError);
    }
}
