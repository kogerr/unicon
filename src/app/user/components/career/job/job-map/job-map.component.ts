import { Component, Input } from '@angular/core';
import { PositionMap } from '../../../../../shared/models/position-map.interface';

@Component({
    selector: 'app-job-map',
    styleUrls: ['./job-map.component.scss'],
    templateUrl: './job-map.component.html',
})
export class JobMapComponent {
    @Input() input?: PositionMap;

    isEmail(text: string): boolean {
        return /\S+@\S+\.\S+/.test(text);
    }
}
