import { Component, Input } from '@angular/core';
import { PositionList } from '../../../../../shared/models/position-list.interface';

@Component({
    selector: 'app-job-list',
    styleUrls: ['./job-list.component.scss'],
    templateUrl: './job-list.component.html',
})
export class JobListComponent {
    @Input() input?: PositionList;
}
