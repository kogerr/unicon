import { Component, Input } from '@angular/core';
import { PositionText } from '../../../../../shared/models/position-text.interface';

@Component({
    selector: 'app-job-text',
    styleUrls: ['./job-text.component.scss'],
    templateUrl: './job-text.component.html',
})
export class JobTextComponent {
    @Input() input?: PositionText;
}
