import { Component, Input } from '@angular/core';
import { Position } from '../../../../shared/models/position.interface';

@Component({
    selector: 'app-job',
    styleUrls: ['./job.component.scss'],
    templateUrl: './job.component.html',
})
export class JobComponent {
    @Input() position?: Position;
    open = false;
}
