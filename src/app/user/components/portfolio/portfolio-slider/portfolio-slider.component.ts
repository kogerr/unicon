import { Component } from '@angular/core';
import { TitledText } from '../../../../shared/models/titled-text.interface';
import { HttpService } from '../../../../shared/services/http.service';
import { CircularList } from '../../philosophy/about-us-slider/circular.list';

@Component({
    selector: 'app-portfolio-slider',
    styleUrls: ['./portfolio-slider.component.scss'],
    templateUrl: './portfolio-slider.component.html',
})
export class PortfolioSliderComponent {
    slides: CircularList<TitledText> = new CircularList<TitledText>([]);

    constructor(private httpService: HttpService) {
        httpService.getPortfolioSlides()
            .subscribe(this.initializeSliders, console.error);
    }

    handleClick(event: 'left' | 'right'): void {
        if (event === 'left') {
            this.slides.previous();
        } else if (event === 'right') {
            this.slides.next();
        }
    }

    private initializeSliders = (slides: TitledText[]) => this.slides =  new CircularList<TitledText>(slides);
}
