import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-next-button',
    styleUrls: ['./next-button.component.scss'],
    template: `<button [routerLink]="link" [class]="colourClass">Tovább ></button>`,
})
export class NextButtonComponent {
    @Input() colourClass = '';
    @Input() link = '';
}
