import { Component } from '@angular/core';

@Component({
    selector: 'app-up-arrow',
    styleUrls: ['./up-arrow.component.scss'],
    templateUrl: './up-arrow.component.html',
})
export class UpArrowComponent {
    scrollUp(): void {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }
}
