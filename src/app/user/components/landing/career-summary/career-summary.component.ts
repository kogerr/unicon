import { Component, OnInit } from '@angular/core';
import { Position } from '../../../../shared/models/position.interface';
import { HttpService } from '../../../../shared/services/http.service';

@Component({
    selector: 'app-career-summary',
    styleUrls: ['./career-summary.component.scss'],
    templateUrl: './career-summary.component.html',
})
export class CareerSummaryComponent implements OnInit {
    positions: string[] = [];

    constructor(private httpService: HttpService) {
    }

    ngOnInit(): void {
        this.httpService.getPositions().subscribe((positions: Position[]) => {
            this.positions = positions.map(position => position.title);
        });
    }
}
