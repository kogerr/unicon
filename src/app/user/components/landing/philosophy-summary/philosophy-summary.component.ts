import { Component } from '@angular/core';

@Component({
    selector: 'app-philosophy-summary',
    styleUrls: ['./philosophy-summary.component.scss'],
    templateUrl: './philosophy-summary.component.html',
})
export class PhilosophySummaryComponent {
    tiles: Array<{ image: string, text: string }> = [
        { image: '/assets/tiles/trust.svg', text: 'bizalom' },
        { image: '/assets/tiles/ethics.svg', text: 'etika' },
        { image: '/assets/tiles/pro.svg', text: 'professzionalizmus' },
        { image: '/assets/tiles/quality.svg', text: 'minőség és maximalizmus' },
        { image: '/assets/tiles/xp.svg', text: 'tapasztalat' },
        { image: '/assets/tiles/study.svg', text: 'tanulás' },
        { image: '/assets/tiles/partners.svg', text: 'partnerség' },
        { image: '/assets/tiles/value.svg', text: 'érték' },
    ];
}
