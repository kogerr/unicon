import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-ps-tile',
    styleUrls: ['./ps-tile.component.scss'],
    templateUrl: './ps-tile.component.html',
})
export class PsTileComponent {
    @Input() image = '';
    @Input() text = '';
}
