import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-summary-header',
    styleUrls: ['./summary-header.component.scss'],
    templateUrl: './summary-header.component.html',
})
export class SummaryHeaderComponent {
    @Input() buttonColourClass = '';
    @Input() link = '';
    @Input() logo = '';
}
