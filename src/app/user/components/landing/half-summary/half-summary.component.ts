import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-half-summary',
    styleUrls: ['./half-summary.component.scss'],
    templateUrl: './half-summary.component.html',
})
export class HalfSummaryComponent {
    @Input() buttonColourClass = '';
    @Input() link = '';
    @Input() logo = '';
}
