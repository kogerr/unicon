import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-summary-logo',
    styleUrls: ['./summary-logo.component.scss'],
    template: '<img [src]="image" alt="logo" class="prlx prlx-1">',
})
export class SummaryLogoComponent {
    @Input() image = '';
}
