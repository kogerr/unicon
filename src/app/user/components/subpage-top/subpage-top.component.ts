import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-subpage-top',
    styleUrls: ['./subpage-top.component.scss'],
    templateUrl: './subpage-top.component.html',
})
export class SubpageTopComponent {
    @Input() logo = '';
}
