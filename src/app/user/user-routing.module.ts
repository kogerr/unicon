import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../shared/components/page-not-found/page-not-found.component';
import { CareerComponent } from './components/career/career.component';
import { ContactComponent } from './components/contact/contact.component';
import { ExperienceComponent } from './components/experience/experience.component';
import { LandingComponent } from './components/landing/landing.component';
import { PhilosophyComponent } from './components/philosophy/philosophy.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';

const routes: Routes = [
    { path: '', component: LandingComponent },
    { path: 'philosophy',  component: PhilosophyComponent },
    { path: 'portfolio',  component: PortfolioComponent },
    { path: 'experience',  component: ExperienceComponent },
    { path: 'contact',  component: ContactComponent },
    { path: 'career',  component: CareerComponent },
    { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)],
})
export class UserRoutingModule { }
