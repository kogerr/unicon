export interface CoverView {
    image: string;
    mobileImage?: string;
    text?: string;
}
