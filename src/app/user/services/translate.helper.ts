const NO_TRANSLATE = 'translateY(0px)';

const settle = (element: HTMLElement) => {
    element.style.transform = NO_TRANSLATE;
};

const findPositionY = (element: HTMLElement): number => {
    let ySum = 0;

    do {
        ySum += element.offsetTop;
        element = (element.offsetParent as HTMLElement);
    } while (element);

    return ySum;
};

const getTransform = (windowCentre: number) => (element: HTMLElement): void => {
    const centerY = window.scrollY + windowCentre;
    const elementY = findPositionY(element);
    const distanceY = elementY - centerY;

    element.style.transform = `translateY(${ distanceY / 30 }px)`;
};

const applyToCollection = (collection: HTMLCollection, callback: (element: HTMLElement) => void): void => {
    for (let i = 0; i < collection.length; i++) {
        callback(collection[i] as HTMLElement);
    }
};

const isAtEnd = () => {
    const documentHeight = document.documentElement.scrollHeight;
    const bodyHeight = document.body.scrollHeight;
    const height = documentHeight > bodyHeight ? documentHeight : bodyHeight;

    return height - window.innerHeight === window.scrollY;
};

export const addAll = () => {
    const elements: HTMLCollection = document.getElementsByClassName('prlx');
    const windowCentre = window.innerHeight / 2;
    const transitionListener = (): void => applyToCollection(elements, getTransform(windowCentre));

    window.addEventListener<'load'>('load', transitionListener);

    window.addEventListener<'scroll'>('scroll', () => {
        if(isAtEnd()) {
            applyToCollection(elements, settle);
        } else {
            requestAnimationFrame(transitionListener);
        }
    });
};
