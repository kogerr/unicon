import { EventEmitter, Injectable, Output } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { CoverView } from '../models/cover-view';

@Injectable()
export class CoverService {
    @Output() change: EventEmitter<CoverView> = new EventEmitter();

    covers: Map<string, CoverView> = new Map([
        [ '/philosophy', { image: 'philosophy.svg', mobileImage: 'philosophy-mobile.svg' } ],
        [ '/portfolio', { image: 'portfolio.svg', mobileImage: 'portfolio-mobile.svg' } ],
        [ '/experience', { image: 'experience.svg', mobileImage: 'experience-mobile.svg' } ],
        [ '/contact', { image: 'contact.svg', mobileImage: 'contact-mobile.svg' } ],
        [ '/career', { image: 'career.svg', mobileImage: 'career-mobile.svg' } ],
        [ '/', { image: 'landing.svg', mobileImage: 'landing-mobile.svg', text: 'uniCon Consulting' } ],
    ]);

    constructor(public router: Router) {
        router.events.subscribe(event => {
            if(event instanceof NavigationStart) {
                const newCover = this.covers.get(event.url) || { image: '' };
                this.change.emit(newCover);
            }
        });
    }
}
