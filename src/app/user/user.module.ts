import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { CareerComponent } from './components/career/career.component';
import { JobListComponent } from './components/career/job/job-list/job-list.component';
import { JobMapComponent } from './components/career/job/job-map/job-map.component';
import { JobTextComponent } from './components/career/job/job-text/job-text.component';
import { JobComponent } from './components/career/job/job.component';
import { ContactComponent } from './components/contact/contact.component';
import { CoverComponent } from './components/cover/cover.component';
import { ExperiencePieComponent } from './components/experience/experience-pie/experience-pie.component';
import { ExperienceComponent } from './components/experience/experience.component';
import { CareerSummaryComponent } from './components/landing/career-summary/career-summary.component';
import { ContactSummaryComponent } from './components/landing/contact-summary/contact-summary.component';
import { ExperienceSummaryComponent } from './components/landing/experience-summary/experience-summary.component';
import { HalfSummaryComponent } from './components/landing/half-summary/half-summary.component';
import { LandingComponent } from './components/landing/landing.component';
import { NextButtonComponent } from './components/landing/next-button/next-button.component';
import { PhilosophySummaryComponent } from './components/landing/philosophy-summary/philosophy-summary.component';
import { PortfolioSummaryComponent } from './components/landing/portfolio-summary/portfolio-summary.component';
import { PsTileComponent } from './components/landing/ps-tile/ps-tile.component';
import { SummaryHeaderComponent } from './components/landing/summary-header/summary-header.component';
import { SummaryLogoComponent } from './components/landing/summary-logo/summary-logo.component';
import { UpArrowComponent } from './components/landing/up-arrow/up-arrow.component';
import { AboutUsSliderComponent } from './components/philosophy/about-us-slider/about-us-slider.component';
import { SliderButtonsComponent } from './components/philosophy/about-us-slider/slider-buttons/slider-buttons.component';
import { SliderHeaderComponent } from './components/philosophy/about-us-slider/slider-header/slider-header.component';
import { PhilosophyTileSeparatorComponent } from './components/philosophy/philosophy-tile-separator/philosophy-tile-separator.component';
import { PhilosophyTileComponent } from './components/philosophy/philosophy-tile/philosophy-tile.component';
import { PhilosophyComponent } from './components/philosophy/philosophy.component';
import { PortfolioSliderComponent } from './components/portfolio/portfolio-slider/portfolio-slider.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { SubpageTopComponent } from './components/subpage-top/subpage-top.component';
import { CoverService } from './services/cover.service';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
    declarations: [
        PhilosophyComponent,
        LandingComponent,
        CoverComponent,
        SummaryLogoComponent,
        NextButtonComponent,
        SummaryHeaderComponent,
        PhilosophySummaryComponent,
        PsTileComponent,
        PortfolioSummaryComponent,
        ExperienceSummaryComponent,
        HalfSummaryComponent,
        ContactSummaryComponent,
        CareerSummaryComponent,
        UpArrowComponent,
        SubpageTopComponent,
        PhilosophyTileComponent,
        PhilosophyTileSeparatorComponent,
        AboutUsSliderComponent,
        SliderHeaderComponent,
        SliderButtonsComponent,
        PortfolioComponent,
        PortfolioSliderComponent,
        ExperienceComponent,
        ExperiencePieComponent,
        ContactComponent,
        CareerComponent,
        JobComponent,
        JobTextComponent,
        JobMapComponent,
        JobListComponent,
    ],
    exports: [
        CoverComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        UserRoutingModule,
    ],
    providers: [
        CoverService,
    ],
})
export class UserModule {
}
