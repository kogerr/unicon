import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const routes = [
    { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
})
export class AppRoutingModule { }
