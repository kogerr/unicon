import { Injectable } from '@angular/core';
import * as decode from 'jwt-decode';
import { DecodedToken } from '../models/decoded-token.interface';

@Injectable()
export class AuthService {

    constructor() {
    }

    redirectUrl!: string;

    saveToken(token: string): void {
        const decodedToken: DecodedToken = decode(token);
        const expiresAt = (decodedToken.exp * 1000).toString();
        const isAdmin = String(decodedToken.admin);

        localStorage.setItem('id_token', token);
        localStorage.setItem('is_admin', isAdmin);
        localStorage.setItem('expires_at', expiresAt);
        localStorage.setItem('email_address', decodedToken.sub);
    }

    logout(): void {
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
    }

    isLoggedIn(): boolean {
        const expiresAt = localStorage.getItem('expires_at');

        return expiresAt !== null && Date.now() < JSON.parse(expiresAt);
    }

    getToken(): string | null {
        return localStorage.getItem('id_token');
    }
}
