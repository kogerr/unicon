import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        const idToken = this.authService.getToken();

        if (idToken) {
            const cloned = req.clone({ headers: req.headers.set('Authorization', `Bearer ${idToken}`) });

            return next.handle(cloned);
        } else {
            return next.handle(req);
        }
    }
}
