import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Headers } from '../../shared/models/headers.interface';
import { Message } from '../../shared/models/message.interface';
import { PlainText } from '../../shared/models/plain-text.interface';
import { Position } from '../../shared/models/position.interface';
import { SuccessFlag } from '../../shared/models/success-flag.interface';
import { TitledText } from '../../shared/models/titled-text.interface';
import { AllowedFlag } from '../models/allowed-flag.interface';
import { LoginResponse } from '../models/login-response.interface';
import { Update } from '../models/update.interface';

const messagesEndpoint = 'api/messages';
const aboutUsEndpoint = 'api/about-us';
const portfolioSlidesEndpoint = 'api/portfolio-slides';
const positionsEndpoint = 'api/positions';
const registrationEndpoint = 'api/register';

@Injectable()
export class AuthApiService {

    constructor(private http: HttpClient) {
    }

    login(email: string, password: string): Observable<LoginResponse> {
        return this.http.post<LoginResponse>('api/login', { email, password });
    }

    register(email: string, password: string): Observable<LoginResponse> {
        return this.http.post<LoginResponse>(registrationEndpoint, { email, password });
    }

    getHeaders(): Observable<Headers> {
        return this.http.post<Headers>('api/headers', {});
    }

    getMessages(): Observable<Message[]> {
        return this.http.get<{ messages: Message[] }>(messagesEndpoint).pipe(map(m => m.messages));
    }

    deleteMessage(message: Message) {
        return this.http.delete<SuccessFlag>(messagesEndpoint + '/' + message._id);
    }

    addAboutUsSlide(slide: PlainText, indices: Array<Update<PlainText>>): Observable<PlainText[]> {
        return this.http.post<PlainText[]>(aboutUsEndpoint, { slide, indices });
    }

    updateAboutUsSlides(updates: Array<Update<PlainText>>): Observable<PlainText[]> {
        return this.http.patch<PlainText[]>(aboutUsEndpoint, updates);
    }

    deleteAboutUsSlide(slide: PlainText): Observable<PlainText[]> {
        return this.http.delete<PlainText[]>(aboutUsEndpoint + '/' + slide._id);
    }

    addPortfolioSlide(slide: TitledText, indices: Array<Update<TitledText>>): Observable<TitledText[]> {
        return this.http.post<TitledText[]>(portfolioSlidesEndpoint, { slide, indices });
    }

    updatePortfolioSlides(updates: Array<Update<TitledText>>): Observable<TitledText[]> {
        return this.http.patch<TitledText[]>(portfolioSlidesEndpoint, updates);
    }

    deletePortfolioSlide(slide: TitledText): Observable<TitledText[]> {
        return this.http.delete<TitledText[]>(portfolioSlidesEndpoint + '/' + slide._id);
    }

    addPosition(position: Position): Observable<Position[]> {
        return this.http.post<Position[]>(positionsEndpoint, position);
    }

    updatePositions(updates: Array<Update<Position>>): Observable<Position[]> {
        return this.http.patch<Position[]>(positionsEndpoint, updates);
    }

    deletePosition(position: Position): Observable<Position[]> {
        return this.http.delete<Position[]>(positionsEndpoint + '/' + position._id);
    }

    setRegistrationFlag(allowedFlag: AllowedFlag): Observable<AllowedFlag> {
        return this.http.patch<AllowedFlag>(registrationEndpoint, allowedFlag);
    }

    checkRegistrationFlag(): Observable<AllowedFlag> {
        return this.http.get<AllowedFlag>(registrationEndpoint);
    }
}
