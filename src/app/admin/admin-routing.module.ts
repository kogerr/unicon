import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsSlidesComponent } from './components/about-us-slides/about-us-slides.component';
import { CareerEditorComponent } from './components/career-editor/career-editor.component';
import { LoginComponent } from './components/login/login.component';
import { MessagesComponent } from './components/messages/messages.component';
import { PortfolioSlidesComponent } from './components/portfolio-slides/portfolio-slides.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
    { path: '', redirectTo: 'messages', pathMatch: 'full' },
    { path: 'messages', component: MessagesComponent, canActivate: [ AuthGuard ]  },
    { path: 'about-us-slides', component: AboutUsSlidesComponent, canActivate: [ AuthGuard ]  },
    { path: 'portfolio-slides', component: PortfolioSlidesComponent, canActivate: [ AuthGuard ]  },
    { path: 'positions', component: CareerEditorComponent, canActivate: [ AuthGuard ]  },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)],
})
export class AdminRoutingModule {
}
