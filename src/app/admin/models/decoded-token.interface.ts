export interface DecodedToken {
    admin: boolean;
    exp: number;
    iat: number;
    sub: string;
}
