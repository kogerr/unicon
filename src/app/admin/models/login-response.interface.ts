export interface LoginResponse {
    error?: unknown;
    success: boolean;
    token?: string;
}
