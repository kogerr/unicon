export interface Update<T> {
    id: string;
    update: Partial<T>;
}
