import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { AdminRoutingModule } from './admin-routing.module';
import { AboutUsSlidesComponent } from './components/about-us-slides/about-us-slides.component';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { CareerEditorComponent } from './components/career-editor/career-editor.component';
import { AddSectionComponent } from './components/career-editor/position-editor/add-section/add-section.component';
import { ListEditorComponent } from './components/career-editor/position-editor/list-editor/list-editor.component';
import { MapEditorComponent } from './components/career-editor/position-editor/map-editor/map-editor.component';
import { PositionEditorComponent } from './components/career-editor/position-editor/position-editor.component';
import { SectionControlsComponent } from './components/career-editor/position-editor/section-controls/section-controls.component';
import { TextEditorComponent } from './components/career-editor/position-editor/text-editor/text-editor.component';
import { TitleEditorComponent } from './components/career-editor/position-editor/title-editor/title-editor.component';
import { LoginComponent } from './components/login/login.component';
import { MessagesComponent } from './components/messages/messages.component';
import { PortfolioSlidesComponent } from './components/portfolio-slides/portfolio-slides.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthApiService } from './services/auth-api.service';
import { AuthGuard } from './services/auth-guard.service';
import { AuthInterceptor } from './services/auth.interceptor';
import { AuthService } from './services/auth.service';

@NgModule({
    declarations: [
        AboutUsSlidesComponent,
        AddSectionComponent,
        AdminMenuComponent,
        CareerEditorComponent,
        ListEditorComponent,
        LoginComponent,
        MapEditorComponent,
        MessagesComponent,
        PortfolioSlidesComponent,
        PositionEditorComponent,
        RegistrationComponent,
        SectionControlsComponent,
        TextEditorComponent,
        TitleEditorComponent,
    ],
    imports: [
        AdminRoutingModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        SharedModule,
    ],
    providers: [ {
        multi: true,
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
    }, AuthApiService, AuthGuard, AuthService ],
})
export class AdminModule {
}
