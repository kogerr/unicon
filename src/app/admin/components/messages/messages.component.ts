import { Component } from '@angular/core';
import { Message } from '../../../shared/models/message.interface';
import { SuccessFlag } from '../../../shared/models/success-flag.interface';
import { AuthApiService } from '../../services/auth-api.service';

@Component({
    selector: 'app-messages',
    styleUrls: ['./messages.component.scss'],
    templateUrl: './messages.component.html',
})
export class MessagesComponent {
    messages: Message[] = [];

    constructor(private api: AuthApiService) {
        this.updateMessages();
    }

    private updateMessages(): void {
        this.api.getMessages().subscribe(next => this.messages = next);
    }

    parseDate(epoch: number): string {
        return new Date(epoch).toDateString();
    }

    delete(message: Message) {
        this.api.deleteMessage(message).subscribe((next: SuccessFlag) => {
            if(next.error) {
                console.error(next.error);
            }
            this.updateMessages();
        }, console.error);
    }
}
