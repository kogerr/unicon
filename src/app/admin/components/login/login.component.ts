import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginResponse } from '../../models/login-response.interface';
import { AuthApiService } from '../../services/auth-api.service';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'app-login',
    styleUrls: ['./login.component.scss'],
    templateUrl: './login.component.html',
})
export class LoginComponent {
    email!: string;
    error: unknown | undefined;
    password!: string;

    constructor(private authApi: AuthApiService, private authService: AuthService, private router: Router) {
    }

    login(): void {
        const next = (res: LoginResponse) => {
            if (res.success) {
                this.authService.saveToken(res.token as string);
                const redirectUrl = this.authService.redirectUrl || '/admin';
                this.router.navigate([ redirectUrl ]);
            }
        };

        const err = (error: unknown) => this.error = error;

        this.authApi.login(this.email, this.password).subscribe(next, err);
    }

    stringify(object: unknown): string {
        return JSON.stringify(object);
    }
}
