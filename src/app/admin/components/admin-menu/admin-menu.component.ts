import { Component } from '@angular/core';
import { AllowedFlag } from '../../models/allowed-flag.interface';
import { AuthApiService } from '../../services/auth-api.service';

@Component({
    selector: 'app-admin-menu',
    styleUrls: ['./admin-menu.component.scss'],
    templateUrl: './admin-menu.component.html',
})
export class AdminMenuComponent {
    registrationAllowed = false;

    constructor(private authApi: AuthApiService) {
        this.authApi.checkRegistrationFlag()
            .subscribe((isAllowed: AllowedFlag) => this.registrationAllowed = isAllowed.allowed);
    }

    setRegistration(inputElement: HTMLInputElement) {
        this.authApi.setRegistrationFlag({ allowed: inputElement.checked })
            .subscribe((isAllowed: AllowedFlag) => this.registrationAllowed = isAllowed.allowed);
    }
}
