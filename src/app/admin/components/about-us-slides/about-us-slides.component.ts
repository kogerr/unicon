import { Component } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { PlainText } from '../../../shared/models/plain-text.interface';
import { HttpService } from '../../../shared/services/http.service';
import { Update } from '../../models/update.interface';
import { AuthApiService } from '../../services/auth-api.service';

@Component({
    selector: 'app-about-us-slides',
    styleUrls: ['./about-us-slides.component.scss'],
    templateUrl: './about-us-slides.component.html',
})
export class AboutUsSlidesComponent {
    slides: PlainText[] = [];

    constructor(private api: AuthApiService, private httpService: HttpService) {
        this.loadSlides();
    }

    delete(slide: PlainText) {
        if (slide._id) {
            const next = (updatedSlides: PlainText[]) => this.slides = updatedSlides;

            this.api.deleteAboutUsSlide(slide).subscribe(next, this.onError);
        } else {
            this.slides.splice(this.slides.findIndex(s => s === slide), 1);

            for(let i = 0; i < this.slides.length; i++) {
                this.slides[i].index = i;
            }
        }
    }

    swap(firstIndex: number, secondIndex: number): void {
        if (firstIndex >= 0 && secondIndex < this.slides.length) {
            this.slides[firstIndex].index = secondIndex;
            this.slides[secondIndex].index = firstIndex;

            this.updateIndices();
        }
    }

    save(slide: PlainText): void {
        if (slide._id) {
            const updates: Array<Update<PlainText>> = [ { id: slide._id, update: { text: slide.text } } ];

            this.api.updateAboutUsSlides(updates).subscribe(this.onSuccess, this.onError);
        } else {
            const indices: Array<Update<PlainText>> = this.slides
                .filter(s => s.index)
                .map(s => Object({ id: s._id, update: { index: s.index } }));

            this.api.addAboutUsSlide(slide, indices).subscribe(this.onSuccess, this.onError);
        }
    }

    newSlide(): void {
        this.slides = [ { text: '', edit: true, index: 0 }, ...this.slides ];

        for(let i = 0; i < this.slides.length; i++) {
            this.slides[i].index = i;
        }
    }

    private loadSlides(): Subscription {
        return this.httpService.getAboutUsSlides().subscribe(this.onSuccess, this.onError);
    }

    private updateIndices(): void {
        const updates: Array<Update<PlainText>> = this.slides
            .filter(slide => slide.index !== undefined)
            .map(slide => Object({ id: slide._id, update: { index: slide.index } }));

        this.api.updateAboutUsSlides(updates).subscribe(this.onSuccess, this.onError);
    }

    private onSuccess = (updatedSlides: PlainText[]) => this.slides = updatedSlides.sort((a, b) => a.index - b.index);

    private onError = (error: unknown) => { console.error(error); this.loadSlides(); };
}
