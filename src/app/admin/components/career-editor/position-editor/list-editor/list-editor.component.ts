import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PositionList } from '../../../../../shared/models/position-list.interface';

@Component({
    selector: 'app-list-editor',
    styleUrls: ['./list-editor.component.scss'],
    templateUrl: './list-editor.component.html',
})
export class ListEditorComponent {
    @Input() content: PositionList = { type: 'list', title: '', content: [], class: '' };
    @Output() contentChange = new EventEmitter<PositionList>();
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    isEdited = false;
    private cachedUpdates = new Array<string>();

    swap(firstIndex: number, secondIndex: number): void {
        if (firstIndex >= 0 && secondIndex < this.content.content.length) {
            [ this.content.content[firstIndex], this.content.content[secondIndex] ] =
                [ this.content.content[secondIndex], this.content.content[firstIndex] ];
        }
    }

    save(): void {
        this.content.content = this.content.content.map((value: string, index: number) => {
            if(this.cachedUpdates[index]) {
                return this.cachedUpdates[index];
            } else {
                return value;
            }
        });

        this.contentChange.emit(this.content);
    }

    cache(index: number, item: string): void {
        this.cachedUpdates[index] = item;
    }

    deleteItem(index: number) {
        this.cachedUpdates.splice(index, 1);
        this.content.content.splice(index, 1);
    }

    newItem(): void {
        this.content.content.push('');
    }
}
