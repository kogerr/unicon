import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PositionMap } from '../../../../../shared/models/position-map.interface';

@Component({
    selector: 'app-map-editor',
    styleUrls: ['./map-editor.component.scss'],
    templateUrl: './map-editor.component.html',
})
export class MapEditorComponent {
    @Input() content: PositionMap = { type: 'map', title: '', content: [], class: '' };
    @Output() contentChange = new EventEmitter<PositionMap>();
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    isEdited = false;
    private cachedUpdates = new Array<{ left: string, right: string[]}>();

    swap(firstIndex: number, secondIndex: number): void {
        if (firstIndex >= 0 && secondIndex < this.content.content.length) {
            [ this.content.content[firstIndex], this.content.content[secondIndex] ] =
                [ this.content.content[secondIndex], this.content.content[firstIndex] ];
        }
    }

    deleteItem(index: number): void {
        this.cachedUpdates.splice(index, 1);
        this.content.content.splice(index, 1);
    }

    newValue(index: number): void {
        this.content.content[index].right.push('');
    }

    deleteValue(itemIndex: number, valueIndex: number): void {
        if(this.cachedUpdates[itemIndex]) {
            this.cachedUpdates[itemIndex].right.splice(valueIndex, 1);
        }
        this.content.content[itemIndex].right.splice(valueIndex, 1);
    }

    newItem(): void {
        this.content.content.push({ left: '', right: [ '' ] });
    }

    cache(itemIndex: number, item: { left: string, right: string[]}, valueIndex: number, value: string): void {
        if(this.cachedUpdates[itemIndex] === undefined) {
            this.cachedUpdates[itemIndex] = { left: item.left, right: [ ...item.right ] };
        }
        this.cachedUpdates[itemIndex].right[valueIndex] = value;
        console.log(this.cachedUpdates);
    }

    save(): void {
        this.content.content = this.content.content.map((value: { left: string, right: string[]}, index: number) => {
            if(this.cachedUpdates[index]) {
                return this.cachedUpdates[index];
            } else {
                return value;
            }
        });

        this.contentChange.emit(this.content);
    }
}
