import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PositionText } from '../../../../../shared/models/position-text.interface';

@Component({
  selector: 'app-text-editor',
    styleUrls: ['./text-editor.component.scss'],
    templateUrl: './text-editor.component.html',
})
export class TextEditorComponent {
    @Input() content: PositionText = { type: 'text', content: '', class: '' };
    @Output() contentChange = new EventEmitter<PositionText>();
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    isEdited = false;
}
