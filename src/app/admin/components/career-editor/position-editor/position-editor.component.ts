import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PositionList } from '../../../../shared/models/position-list.interface';
import { PositionMap } from '../../../../shared/models/position-map.interface';
import { PositionText } from '../../../../shared/models/position-text.interface';
import { Position } from '../../../../shared/models/position.interface';

@Component({
  selector: 'app-position-editor',
    styleUrls: ['./position-editor.component.scss'],
    templateUrl: './position-editor.component.html',
})
export class PositionEditorComponent {
    @Input() position: Position = { index: 0, title: 'New Position', content: [] };
    open = true;
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    @Output() save = new EventEmitter<Partial<Position>>();

    newSection(type: 'text' | 'list' | 'map'): void {
        let newSection: PositionText | PositionList | PositionMap;

        switch (type) {
            case 'text':
                newSection = { type: 'text', content: '', class: 'grey' };
                break;
            case 'list':
                newSection = { type, title: 'New Section', content: [], class: 'grey' } as unknown as PositionList;
                break;
            case 'map':
            default:
                newSection = { type: 'map', title: 'New Section', content: [], class: 'grey' } as unknown as PositionMap;
                break;
        }

        this.position.content = [ newSection, ...this.position.content ];
    }

    deleteSection(index: number) {
        this.position.content.splice(index, 1);
        this.save.emit({ content: this.position.content });
    }

    moveSection(direction: 'up' | 'down', index: number) {
        if(direction === 'up' && index > 0) {
            this.swapSections(index - 1, index);
        } else if(direction === 'down' && index < this.position.content.length - 1) {
            this.swapSections(index, index + 1);
        }
    }

    private swapSections(firstIndex: number, secondIndex: number): void {
        [ this.position.content[firstIndex], this.position.content[secondIndex] ] =
            [ this.position.content[secondIndex], this.position.content[firstIndex] ];

        this.save.emit({ content: this.position.content });
    }
}
