import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-section',
    styleUrls: ['./add-section.component.scss'],
    templateUrl: './add-section.component.html',
})
export class AddSectionComponent {
    @Output() new = new EventEmitter<'text' | 'list' | 'map'>();
}
