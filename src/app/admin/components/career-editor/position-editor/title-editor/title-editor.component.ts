import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-title-editor',
    styleUrls: ['./title-editor.component.scss'],
    templateUrl: './title-editor.component.html',
})
export class TitleEditorComponent {
    @Input() title = '';
    @Input() open = false;
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    @Output() save = new EventEmitter<string>();
    @Output() openChange = new EventEmitter<boolean>();
    isEdited = false;
}
