import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-section-controls',
    styleUrls: ['./section-controls.component.scss'],
    templateUrl: './section-controls.component.html',
})
export class SectionControlsComponent {
    @Input() color = 'light-grey';
    @Output() colorChange = new EventEmitter<string>();
    @Input() isEdited = false;
    @Output() isEditedChange = new EventEmitter<boolean>();
    @Output() delete = new EventEmitter<boolean>();
    @Output() move = new EventEmitter<'up' | 'down'>();
    @Output() save = new EventEmitter<boolean>();
}
