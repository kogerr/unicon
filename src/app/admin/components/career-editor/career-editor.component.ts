import { Component } from '@angular/core';
import { Position } from '../../../shared/models/position.interface';
import { HttpService } from '../../../shared/services/http.service';
import { Update } from '../../models/update.interface';
import { AuthApiService } from '../../services/auth-api.service';

@Component({
    selector: 'app-career-editor',
    styleUrls: ['./career-editor.component.scss'],
    templateUrl: './career-editor.component.html',
})
export class CareerEditorComponent {
    positions: Position[] = [];

    constructor(private api: AuthApiService, private httpService: HttpService) {
        this.loadPositions();
    }

    newPosition(): void {
        this.positions = [ { index: 0, title: 'New Position', content: [] }, ...this.positions ];
    }

    deletePosition(position: Position): void {
        this.api.deletePosition(position).subscribe(this.onSuccess, this.onError);
    }

    movePosition($event: 'up' | 'down', index: number): void {
        if($event === 'up' && index > 0) {
            this.switchPositions(index - 1, index);
        } else if($event === 'down' && index < this.positions.length - 1) {
            this.switchPositions(index, index + 1);
        }
    }

    savePosition(position: Position, query: Partial<Position>) {
        if(position._id) {
            const update: Update<Position> = { id: position._id, update: query };
            this.api.updatePositions( [ update ]).subscribe(this.onSuccess, this.onError);
        } else {
            this.api.addPosition({ ...position, ...query }).subscribe(this.onSuccess, this.onError);
        }
    }

    private loadPositions(): void {
        this.httpService.getPositions().subscribe(positions => this.positions = positions);
    }

    private switchPositions(firstIndex: number, secondIndex: number): void {
        this.positions[firstIndex].index = secondIndex;
        this.positions[secondIndex].index = firstIndex;

        const updates: Array<Update<Position>> = this.positions
            .filter(position => position.index !== undefined)
            .map(position => Object({ id: position._id, update: { index: position.index } }));

        this.api.updatePositions(updates).subscribe(this.onSuccess, this.onError);
    }

    private onSuccess = (updatedPositions: Position[]) => this.positions = updatedPositions.sort((a, b) => a.index - b.index);

    private onError = (error: unknown) => { console.error(error); this.loadPositions(); };
}
