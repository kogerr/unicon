import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginResponse } from '../../models/login-response.interface';
import { AuthApiService } from '../../services/auth-api.service';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'app-registration',
    styleUrls: ['./registration.component.scss'],
    templateUrl: './registration.component.html',
})
export class RegistrationComponent {
    email!: string;
    error = '';
    password!: string;

    constructor(private authApi: AuthApiService, private authService: AuthService, private router: Router) {
    }

    register(): void {
        const next = (res: LoginResponse) => {
            if (res.success) {
                this.authService.saveToken(res.token as string);
                this.router.navigate([ '/admin' ]);
            } else if(res.error) {
                this.error = JSON.stringify(res.error);
            }
        };

        const onError = (error: { error: unknown }) => this.error = JSON.stringify(error.error);

        this.authApi.register(this.email, this.password).subscribe(next, onError);
    }
}
