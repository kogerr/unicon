import { Component } from '@angular/core';

@Component({
    selector: 'app-letter',
    styleUrls: ['./letter.component.scss'],
    templateUrl: './letter.component.html',
})
export class LetterComponent {

    scrollDown(event: MouseEvent) {
        event.preventDefault();
        window.scrollTo({ top: 10000, behavior: 'smooth' });
    }
}
