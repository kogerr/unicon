import { Component } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';

const LOCK_TIMEOUT = 1000;

@Component({
    selector: 'app-sidebar',
    styleUrls: ['./sidebar.component.scss'],
    templateUrl: './sidebar.component.html',
})
export class SidebarComponent {
    items: Array<{ title: string, link: string }> = [
        { title: '¹ ― Cégfilozófia', link: 'philosophy' },
        { title: '² ― Szolgáltatás portfólió', link: 'portfolio' },
        { title: '³ ― Tapasztalat', link: 'experience' },
        { title: '⁴ ― Kapcsolat', link: 'contact' },
        { title: '⁵ ― Karrier', link: 'career' },
    ];
    open = false;
    private locked = false;

    constructor(sidebarService: SidebarService) {
        sidebarService.change.subscribe((open: boolean) => this.setOpen(open));
    }

    setOpen(open: boolean): void {
        if (!this.locked) {
            this.open = open;
            if (!open) {
                this.lock();
            }
        }
    }

    private lock(): void {
        this.locked = true;
        setTimeout(() => this.locked = false, LOCK_TIMEOUT);
    }
}
