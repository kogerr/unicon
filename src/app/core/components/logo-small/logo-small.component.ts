import { Component } from '@angular/core';

@Component({
    selector: 'app-logo-small',
    styleUrls: ['./logo-small.component.scss'],
    templateUrl: './logo-small.component.html',
})
export class LogoSmallComponent {
}
