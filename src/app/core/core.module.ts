import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserRoutingModule } from '../user/user-routing.module';
import { UserModule } from '../user/user.module';
import { HamburgerComponent } from './components/hamburger/hamburger.component';
import { HeaderComponent } from './components/header/header.component';
import { LetterComponent } from './components/letter/letter.component';
import { LogoFullComponent } from './components/logo-full/logo-full.component';
import { LogoSmallComponent } from './components/logo-small/logo-small.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { XComponent } from './components/x/x.component';
import { SidebarService } from './services/sidebar.service';

@NgModule({
    declarations: [
        SidebarComponent,
        LetterComponent,
        HamburgerComponent,
        LogoSmallComponent,
        LogoFullComponent,
        XComponent,
        HeaderComponent,
    ],
    exports: [
        HeaderComponent,
        LetterComponent,
        SidebarComponent,
    ],
    imports: [
        CommonModule,
        UserRoutingModule,
        UserModule,
    ],
    providers: [
        SidebarService,
    ],
})
export class CoreModule {
}
