import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable()
export class SidebarService {
    @Output() change: EventEmitter<boolean> = new EventEmitter();

    setOpen(open: boolean): void {
        this.change.emit(open);
    }
}
