import { Component, OnInit } from '@angular/core';
import { SidebarService } from './core/services/sidebar.service';
import { addAll } from './user/services/translate.helper';

@Component({
    selector: 'app-root',
    styleUrls: ['./app.component.scss'],
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    constructor(private sidebarService: SidebarService) { }

    ngOnInit(): void {
        addAll();
    }

    closeSidebar(): void {
        this.sidebarService.setOpen(false);
    }
}
