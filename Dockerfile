FROM node:10 as builder

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm ci
COPY . .

RUN npm run build
RUN gzip build/dist/*.js
RUN gzip build/dist/*.css
RUN gzip build/dist/assets/* -r

FROM node:10

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/package*.json ./
COPY --from=builder /usr/src/app/build/ ./

RUN npm ci --production

ENV GZIP true

EXPOSE 80
ENTRYPOINT [ "node", "app.js" ]
