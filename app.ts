import { app } from './server/config/express';
import { mongoConnectWithRetry } from './server/config/mongo';
import { listen } from './server/config/server';

listen(app);

mongoConnectWithRetry();
