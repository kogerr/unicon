import * as expressJwt from 'express-jwt';
import * as fs from 'fs';
import { Secret, sign } from 'jsonwebtoken';
import { appName, token } from '../config/properties';

const algorithm = token.algorithm;
const expiresIn = token.expiry;
const passphrase = appName;
const privateKey = fs.readFileSync(token.privateKeyPath, 'utf8');
const publicKey = fs.readFileSync(token.publicKeyPath, 'utf8');

const secretOrPrivateKey: Secret = { key: privateKey, passphrase };

export function issueToken(admin: boolean, email: string): string {
    const payload = { admin: admin ? admin : false };
    const options = { algorithm, expiresIn, subject: email };

    return sign(payload, secretOrPrivateKey, options);
}

export const checkToken: expressJwt.RequestHandler = expressJwt({
    secret: publicKey,
});
