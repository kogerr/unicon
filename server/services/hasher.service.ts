import * as crypto from 'crypto';
import { appName } from '../config/properties';

const algorithm = 'sha256';
const encoding = 'hex';
const salt = appName;

/**
 * Hashes data with Hmac.
 * @param data - data to be hashed
 * @returns the hashed data
 */
export function hash(data: string): string {
    const hmac = crypto.createHmac(algorithm, salt);
    hmac.update(data);

    return hmac.digest(encoding);
}
