import * as bodyParser from 'body-parser';
import * as express from 'express';
import { Express, NextFunction, Request, Response } from 'express';
import { router } from '../routes';

const app: Express = express();

app.use(bodyParser.json());
app.use('/api', router);

if (process.env.GZIP === 'true') {
    app.get('*.js', (req: Request, res: Response, next: NextFunction) => {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'text/javascript');
        next();
    });
    app.get('*.css', (req: Request, res: Response, next: NextFunction) => {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'text/css');
        next();
    });
    app.get('*.svg', (req: Request, res: Response, next: NextFunction) => {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'image/svg+xml');
        next();
    });
    app.get('*.png', (req: Request, res: Response, next: NextFunction) => {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'image/png');
        next();
    });
}

app.use(express.static('dist', { maxAge: '30d' }));

app.use('/.well-known', express.static('certbot/.well-known'));

app.get('/*', (req: Request, res: Response): void => {
    res.sendFile('dist/index.html', { root: '.' });
});

export { app };
