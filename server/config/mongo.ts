import { Collection, Db, MongoClient, MongoClientOptions, MongoError } from 'mongodb';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { appName, mongo } from './properties';

const host = process.env.MONGO_HOST || mongo.host;
const port = mongo.port;
const dbName = appName;
const user = process.env.MONGO_USR;
const password = process.env.MONGO_PWD;
const authMechanism = 'DEFAULT';

let uri = `mongodb://${host}:${port}`;
const options: MongoClientOptions = { useNewUrlParser: true };

if (user !== undefined && password !== undefined && user.length > 0) {
    uri = `mongodb://${user}:${password}@${host}:${port}/?authMechanism=${authMechanism}`;
    console.log(uri);
}

const client = new MongoClient(uri, options);

const dbSubject = new Subject<Db>();

const mongoConnectWithRetry = (tries = 0) => {
    client.connect((error: MongoError, result: MongoClient) => {
        if (error) {
            console.error(error);
            if (tries < mongo.retries) {
                setTimeout(() => mongoConnectWithRetry(tries + 1), mongo.retryInterval);
            }
        } else {
            console.log('Connected to MongoDB');
            dbSubject.next(result.db(dbName));
        }
    });
};

const getCollection = <T>(name: string): Observable<Collection<T>> => {
    return dbSubject.pipe(map((db: Db) => db.collection<T>(name)));
};

export { getCollection, mongoConnectWithRetry };
