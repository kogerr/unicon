const appName = 'unicon';

const certificatePath = './cert/';

const mongo = {
    host: 'localhost',
    port: 27017,
    retries: 3,
    retryInterval: 10000,
};

const token = {
    algorithm: 'RS256',
    expiry: 7200,
    privateKeyPath: './keys/private.pem',
    publicKeyPath: './keys/public.pem',
};

export { appName, certificatePath, mongo, token };
