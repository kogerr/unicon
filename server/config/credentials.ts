import { readFileSync } from 'fs';
import { certificatePath } from './properties';

const certPath = process.env.CERT_PATH || certificatePath;
const encoding = 'utf8';

let key;
let cert;
let ca;

try {
    key = readFileSync(certPath + 'privkey.pem', encoding);
    cert = readFileSync(certPath + 'cert.pem', encoding);
    ca = readFileSync(certPath + 'chain.pem', encoding);
} catch (error) {
    console.error(error);
}

const credentials = key && cert && ca ? { key, cert, ca } : undefined;

export { credentials };
