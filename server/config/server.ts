import { createServer as createHttpServer, IncomingMessage, RequestListener, ServerResponse } from 'http';
import { createServer as createHttpsServer, ServerOptions } from 'https';
import { credentials } from './credentials';

const httpPort = 80;
const httpsPort = 443;
const redirectStatusCode = 301;

const httpsRedirect = (req: IncomingMessage, res: ServerResponse) => {
    res.writeHead(redirectStatusCode, { Location: 'https://' + req.headers.host + req.url });
    res.end();
};

const listenOnHttps = (requestListener: RequestListener, serverOptions: ServerOptions) => {
    createHttpsServer(serverOptions, requestListener)
        .listen(httpsPort, () => {
            console.log(`Listening on port ${httpsPort}`);
        });
};

const listenOnHttp = (requestListener: RequestListener) => {
    createHttpServer(requestListener)
        .listen(httpPort, () => {
            console.log(`Listening on port ${httpPort}`);
        });
};

const listen = (expressApp: RequestListener) => {
    let httpRequestListener: RequestListener;

    if (credentials !== undefined) {
        httpRequestListener = httpsRedirect;
        listenOnHttps(expressApp, credentials);
    } else {
        httpRequestListener = expressApp;
    }

    listenOnHttp(httpRequestListener);
};

export { listen };
