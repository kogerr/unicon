import * as express from 'express';
import { addAboutUsSlide, deleteAboutUsSlide, getAboutUsSlides, updateAboutUsSlides } from '../controllers/about-us.controller';
import { returnHeaders } from '../controllers/headers.controller';
import { checkRegistration, login, register, switchRegistration } from '../controllers/login.controller';
import { addMessage, deleteMessage, getMessages } from '../controllers/message.controller';
import {
    addPortfolioSlide,
    deletePortfolioSlide,
    getPortfolioSlides,
    updatePortfolioSlides
} from '../controllers/portfolio-slides.controller';
import { addPosition, deletePosition, getPositions, updatePositions } from '../controllers/position.controller';
import { checkToken } from '../services/token.service';

const router: express.Router = express.Router();

router.route('/headers')
    .get(returnHeaders)
    .post(checkToken, returnHeaders);

router.route('/login')
    .post(login);

router.route('/register')
    .patch(checkToken, switchRegistration)
    .post(register)
    .get(checkToken, checkRegistration);

router.route('/messages')
    .post(addMessage)
    .get(checkToken, getMessages);
router.route('/messages/:id')
    .delete(checkToken, deleteMessage);

router.route('/about-us')
    .patch(checkToken, updateAboutUsSlides)
    .post(checkToken, addAboutUsSlide)
    .get(getAboutUsSlides);
router.route('/about-us/:id')
    .delete(checkToken, deleteAboutUsSlide);

router.route('/portfolio-slides')
    .patch(checkToken, updatePortfolioSlides)
    .post(checkToken, addPortfolioSlide)
    .get(getPortfolioSlides);
router.route('/portfolio-slides/:id')
    .delete(checkToken, deletePortfolioSlide);

router.route('/positions')
    .patch(checkToken, updatePositions)
    .post(checkToken, addPosition)
    .get(getPositions);
router.route('/positions/:id')
    .delete(checkToken, deletePosition);

export { router };
