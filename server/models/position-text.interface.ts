export interface PositionText {
    type: 'text';
    title: string;
    content: string;
    class: string;
}
