export interface PositionList {
    type: 'list';
    title: string;
    content: string[];
    class: string;
}
