export interface TitledText {
    _id?: string;
    index: number;
    title: string;
    text: string;
}
