export interface PlainText {
    _id?: string;
    index: number;
    text: string;
}
