import { PositionList } from './position-list.interface';
import { PositionMap } from './position-map.interface';
import { PositionText } from './position-text.interface';

export interface Position {
    _id?: string;
    index: number;
    title: string;
    content: Array<PositionText | PositionList | PositionMap>;
}
