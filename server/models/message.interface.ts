export interface Message {
    date: number;
    email: string;
    text: string;
    name: string;
    phone?: string;
}
