import { ObjectId } from 'bson';
import { Collection, DeleteWriteOpResultObject, FilterQuery, InsertOneWriteOpResult, UpdateQuery, UpdateWriteOpResult } from 'mongodb';
import { getCollection } from '../../config/mongo';
import { PlainText } from '../../models/plain-text.interface';
import { Update } from '../../models/update.interface';

let collection: Collection<PlainText>;
getCollection<PlainText>('aboutUsSlides').subscribe(aboutUsSlidesCollection => collection = aboutUsSlidesCollection);

export async function findSlides(): Promise<PlainText[]> {
    return collection.find().toArray();
}

export async function insertSlide(slide: PlainText): Promise<InsertOneWriteOpResult> {
    return collection.insertOne(slide);
}

export async function deleteOneSlide(id: string): Promise<DeleteWriteOpResultObject> {
    return collection.deleteOne({ _id: new ObjectId(id) });
}

export async function updateAllSlides(slides: PlainText[]): Promise<Array<Promise<UpdateWriteOpResult>>> {
    const updates: Array<Promise<UpdateWriteOpResult>> = [];

    const callbackfn = (slide: PlainText) => {
        const filter = { _id: new ObjectId(slide._id) };
        delete slide._id;
        const update = { $set: slide };

        updates.push(collection.updateOne(filter, update));
    };

    slides.forEach(callbackfn);

    return updates;
}

export async function updateSlides(updates: Array<Update<PlainText>>): Promise<Array<Promise<UpdateWriteOpResult>>> {
    const results: Array<Promise<UpdateWriteOpResult>> = [];

    const callbackfn = (query: Update<PlainText>) => {
        const filter: FilterQuery<PlainText> = { _id: new ObjectId(query.id) };
        const updateQuery: UpdateQuery<PlainText> = { $set: query.update };

        results.push(collection.updateOne(filter, updateQuery));
    };

    updates.forEach(callbackfn);

    return results;
}
