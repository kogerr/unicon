import { Collection, FilterQuery, InsertOneWriteOpResult } from 'mongodb';
import { getCollection } from '../../config/mongo';
import { User } from '../../models/user.interface';

let collection: Collection<User>;
getCollection<User>('users').subscribe(userCollection => collection = userCollection);

export async function checkUser(email: string, password: string): Promise<User | null> {
    const filter: FilterQuery<User> = { email, password };

    return collection.findOne(filter);
}

export async function addUser(email: string, password: string): Promise<InsertOneWriteOpResult> {
    const newUser: User = { email, password, admin: false };

    return collection.insertOne(newUser);
}
