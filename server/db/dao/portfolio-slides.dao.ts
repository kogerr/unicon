import { ObjectId } from 'bson';
import { Collection, DeleteWriteOpResultObject, FilterQuery, InsertOneWriteOpResult, UpdateQuery, UpdateWriteOpResult } from 'mongodb';
import { getCollection } from '../../config/mongo';
import { TitledText } from '../../models/titled-text.interface';
import { Update } from '../../models/update.interface';

let collection: Collection<TitledText>;
getCollection<TitledText>('portfolioSlides').subscribe(portfolioSlides => collection = portfolioSlides);

export async function findSlides(): Promise<TitledText[]> {
    return collection.find().toArray();
}

export async function insertSlide(slide: TitledText): Promise<InsertOneWriteOpResult> {
    return collection.insertOne(slide);
}

export async function deleteOneSlide(id: string): Promise<DeleteWriteOpResultObject> {
    return collection.deleteOne({ _id: new ObjectId(id) });
}

export async function updateAllSlides(slides: TitledText[]): Promise<Array<Promise<UpdateWriteOpResult>>> {
    const updates: Array<Promise<UpdateWriteOpResult>> = [];

    const callbackfn = (slide: TitledText) => {
        const filter = { _id: new ObjectId(slide._id) };
        delete slide._id;
        const update = { $set: slide };

        updates.push(collection.updateOne(filter, update));
    };

    slides.forEach(callbackfn);

    return updates;
}

export async function updateSlides(updates: Array<Update<TitledText>>): Promise<Array<Promise<UpdateWriteOpResult>>> {
    const results: Array<Promise<UpdateWriteOpResult>> = [];

    const callbackfn = (query: Update<TitledText>) => {
        const filter: FilterQuery<TitledText> = { _id: new ObjectId(query.id) };
        const updateQuery: UpdateQuery<TitledText> = { $set: query.update };

        results.push(collection.updateOne(filter, updateQuery));
    };

    updates.forEach(callbackfn);

    return results;
}
