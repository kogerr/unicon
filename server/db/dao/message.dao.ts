import { ObjectId } from 'bson';
import { Collection, DeleteWriteOpResultObject, InsertOneWriteOpResult } from 'mongodb';
import { getCollection } from '../../config/mongo';
import { Message } from '../../models/message.interface';

let collection: Collection<Message>;
getCollection<Message>('messages').subscribe(messageCollection => collection = messageCollection);

export async function findMessages(): Promise<Message[]> {
    return collection.find().toArray();
}

export async function insertMessage(message: Message): Promise<InsertOneWriteOpResult> {
    return collection.insertOne(message);
}

export async function deleteOneMessage(id: string): Promise<DeleteWriteOpResultObject> {
    return collection.deleteOne({ _id: new ObjectId(id) });
}
