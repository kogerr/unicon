import {
    Collection,
    DeleteWriteOpResultObject,
    FilterQuery,
    InsertOneWriteOpResult,
    ObjectId,
    UpdateQuery,
    UpdateWriteOpResult
} from 'mongodb';
import { getCollection } from '../../config/mongo';
import { Position } from '../../models/position.interface';
import { Update } from '../../models/update.interface';

let collection: Collection<Position>;
getCollection<Position>('positions').subscribe(positionCollection => collection = positionCollection);

export async function findPositions(): Promise<Position[]> {
    return collection.find().toArray();
}

export async function insertPosition(position: Position): Promise<InsertOneWriteOpResult> {
    return collection.insertOne(position);
}

export async function deleteOne(id: string): Promise<DeleteWriteOpResultObject> {
    return collection.deleteOne({ _id: new ObjectId(id) });
}

export async function updateMany(updates: Array<Update<Position>>): Promise<Array<Promise<UpdateWriteOpResult>>> {
    const results: Array<Promise<UpdateWriteOpResult>> = [];

    const callbackfn = (query: Update<Position>) => {
        const filter: FilterQuery<Position> = { _id: new ObjectId(query.id) };
        const updateQuery: UpdateQuery<Position> = { $set: query.update };

        results.push(collection.updateOne(filter, updateQuery));
    };

    updates.forEach(callbackfn);

    return results;
}
