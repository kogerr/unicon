import { Collection, DeleteWriteOpResultObject, InsertOneWriteOpResult } from 'mongodb';
import { getCollection } from '../../config/mongo';
import { AllowedFlag } from '../../models/allowed-flag.interface';

let collection: Collection<AllowedFlag>;
getCollection<AllowedFlag>('registration').subscribe(registrationCollection => collection = registrationCollection);

export function removeColllection(): Promise<DeleteWriteOpResultObject> {
    return collection.deleteMany({});
}

export async function checkRegistrationFlag(): Promise<AllowedFlag | null> {
    return collection.findOne({});
}

export async function setRegistrationFlag(value: AllowedFlag): Promise<InsertOneWriteOpResult> {
    return collection.insertOne(value);
}
