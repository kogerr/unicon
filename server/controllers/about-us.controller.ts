import { Request, Response } from 'express';
import { DeleteWriteOpResultObject, InsertOneWriteOpResult, UpdateWriteOpResult } from 'mongodb';
import { deleteOneSlide, findSlides, insertSlide, updateAllSlides, updateSlides } from '../db/dao/about-us.dao';
import { PlainText } from '../models/plain-text.interface';
import { Update } from '../models/update.interface';

const onRejectedError = (res: Response) => (reason: unknown) => {
    res.status(500);
    res.send({ success: false, error: reason });
};

const onGet = (res: Response) => (result: PlainText[]) => {
    const slides = result.sort((a, b) => a.index - b.index);
    res.status(200);
    res.send(slides);
};

const onUpdate = (res: Response) => (result: Array<Promise<UpdateWriteOpResult>>) => {
    Promise.all(result).then((updates: UpdateWriteOpResult[]) => {
        const success = updates.every(update => update.result.ok === 1 && update.matchedCount === 1);

        if(success) {
            findSlides().then(onGet(res), onRejectedError(res));
        } else {
            onRejectedError(res)([ updates ]);
        }
    });
};

export function getAboutUsSlides(req: Request, res: Response): void {
    findSlides().then(onGet(res), onRejectedError(res));
}

export function updateAboutUsSlides(req: Request, res: Response): void {
    updateSlides(req.body).then(onUpdate(res), onRejectedError(res));
}

export function addAboutUsSlide(req: Request, res: Response): void {
    const newSlide = req.body.slide;
    const indices = req.body.indices;

    const onFulfilled = (result: InsertOneWriteOpResult) => {
        if (result.result.ok === 1 && result.insertedCount === 1) {
            updateSlides(indices).then(onUpdate(res), onRejectedError(res));
        } else {
            onRejectedError(res)(result);
        }
    };

    insertSlide(newSlide).then(onFulfilled, onRejectedError(res));
}

export function deleteAboutUsSlide(req: Request, res: Response): void {
    const onFind = (result: PlainText[]) => {
        const slides = result.sort((a, b) => a.index - b.index);
        const updates: Array<Update<PlainText>> = [];

        for(let i = 0; i < slides.length; i++) {
            updates.push({ id: slides[i]._id, update: { index: i }} as unknown as Update<PlainText>);
        }

        updateSlides(updates).then(onUpdate(res), onRejectedError(res));
    };

    const onDelete = (result: DeleteWriteOpResultObject) => {
        if (result.result.ok === 1 && result.deletedCount === 1) {
            findSlides().then(onFind, onRejectedError(res));
        } else {
            onRejectedError(res)(result);
        }
    };

    deleteOneSlide(req.params.id).then(onDelete, onRejectedError(res));
}

export function updateAllAboutUsSlides(req: Request, res: Response): void {
    updateAllSlides(req.body).then(onUpdate(res), onRejectedError(res));
}
