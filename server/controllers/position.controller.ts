import { Request, Response } from 'express';
import { DeleteWriteOpResultObject, InsertOneWriteOpResult, ObjectID, UpdateWriteOpResult } from 'mongodb';
import { deleteOne, findPositions, insertPosition, updateMany } from '../db/dao/position.dao';
import { PlainText } from '../models/plain-text.interface';
import { Position } from '../models/position.interface';
import { Update } from '../models/update.interface';

const onRejectedError = (res: Response) => (reason: unknown) => {
    res.status(500);
    res.send({ success: false, error: reason });
};

const onGet = (res: Response) => (result: Position[]) => {
    const slides = result.sort((a, b) => a.index - b.index);
    res.status(200);
    res.send(slides);
};

const onUpdate = (res: Response) => (result: Array<Promise<UpdateWriteOpResult>>) => {
    Promise.all(result).then((updates: UpdateWriteOpResult[]) => {
        const success = updates.every(update => update.result.ok === 1 && update.matchedCount === 1);

        if(success) {
            findPositions().then(onGet(res), onRejectedError(res));
        } else {
            onRejectedError(res)([ updates ]);
        }
    });
};

const onFindUpdateIndices = (res: Response) => (result: Position[]) => {
    const positions = result.sort((a, b) => a.index - b.index);
    const updates: Array<Update<PlainText>> = [];

    for(let i = 0; i < positions.length; i++) {
        updates.push({ id: positions[i]._id, update: { index: i }} as unknown as Update<PlainText>);
    }

    updateMany(updates).then(onUpdate(res), onRejectedError(res));
};

export function getPositions(req: Request, res: Response): void {
    findPositions().then(onGet(res), onRejectedError(res));
}

export function updatePositions(req: Request, res: Response): void {
    updateMany(req.body).then(onUpdate(res), onRejectedError(res));
}

export function addPosition(req: Request, res: Response): void {
    const onFindInsertUpdateIndices = (id: ObjectID) => (result: Position[]) => {
        const positions = result.sort((a, b) => a.index - b.index);
        const updates: Array<Update<PlainText>> = [];

        for(let i = 0; i < positions.length; i++) {
            if(id.equals(positions[i]._id as string)) {
                updates.push({ id: positions[i]._id, update: { index: 0 } } as unknown as Update<PlainText>);
            } else {
                updates.push({ id: positions[i]._id, update: { index: i + 1 } } as unknown as Update<PlainText>);
            }
        }

        updateMany(updates).then(onUpdate(res), onRejectedError(res));
    };

    const onInsert = (result: InsertOneWriteOpResult) => {
        if (result.result.ok === 1) {
            findPositions().then(onFindInsertUpdateIndices(result.insertedId), onRejectedError(res));
        } else {
            onRejectedError(res)(result);
        }
    };

    insertPosition(req.body).then(onInsert, onRejectedError(res));
}

export function deletePosition(req: Request, res: Response): void {
    const onDelete = (result: DeleteWriteOpResultObject) => {
        if (result.result.ok === 1 && result.deletedCount === 1) {
            findPositions().then(onFindUpdateIndices(res), onRejectedError(res));
        } else {
            onRejectedError(res)(result);
        }
    };

    deleteOne(req.params.id).then(onDelete, onRejectedError(res));
}
