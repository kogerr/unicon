import { Request, Response } from 'express';
import { DeleteWriteOpResultObject, InsertOneWriteOpResult } from 'mongodb';
import { checkRegistrationFlag, removeColllection, setRegistrationFlag } from '../db/dao/register.dao';
import { addUser, checkUser } from '../db/dao/user.dao';
import { AllowedFlag } from '../models/allowed-flag.interface';
import { User } from '../models/user.interface';
import { hash } from '../services/hasher.service';
import { issueToken } from '../services/token.service';

export function login(req: Request, res: Response): void {
    const email = req.body.email;
    const hashedPassword = hash(req.body.password);

    const onFulfilled = (user: User | null) => {
        res.statusCode = 200;
        if (user) {
            const token = issueToken(user.admin, user.email);
            res.send({ success: true, token });
        } else {
            res.send({ success: false });
        }
    };

    const onRejected = (error: unknown) => {
        console.error(error);
        res.statusCode = 500;
        res.send({ success: false, error });
    };

    checkUser(email, hashedPassword).then(onFulfilled, onRejected);
}

export function register(req: Request, res: Response): void {
    const onFulfilled = (email: string) => (result: InsertOneWriteOpResult) => {
        res.statusCode = 200;
        if (result.result.ok === 1) {
            const token = issueToken(false, email);
            res.send({ success: true, token });
        } else {
            res.send({ success: false });
        }
    };

    const onRejected = (error: unknown) => {
        console.error(error);
        res.statusCode = 500;
        res.send({ success: false, error });
    };

    const onNotAllowed = () => {
        res.statusCode = 401;
        res.send({ success: false, error: 'Registration not open at the moment.' });
    };

    const onAllowed = (result: AllowedFlag | null) => {
        if(result !== null && result.allowed) {
            const email = req.body.email as unknown as string;
            const hashedPassword = hash(req.body.password);

            addUser(email, hashedPassword).then(onFulfilled(email), onRejected);
        } else {
            onNotAllowed();
        }
    };

    checkRegistrationFlag()
        .then(onAllowed)
        .catch(onRejected);
}

export function switchRegistration(req: Request, res: Response): void {
    const onRejected = (error: unknown) => {
        console.error(error);
        res.statusCode = 500;
        res.send({ allowed: false, error });
    };

    const onLoad = (result: AllowedFlag | null) => {
        res.statusCode = 200;
        if(result) {
            res.send(result);
        } else {
            res.send({ allowed: false });
        }
    };

    const onInsert = (result: InsertOneWriteOpResult) => {
        if (result.result.ok === 1) {
            checkRegistrationFlag()
                .then(onLoad)
                .catch(onRejected);
        } else {
            res.statusCode = 500;
            res.send({ success: false });
        }
    };

    const onReset = () => {
        setRegistrationFlag(req.body)
            .then(onInsert)
            .catch(onRejected);
    };

    removeColllection()
        .then(onReset)
        .catch(onRejected);
}

export function checkRegistration(req: Request, res: Response): void {
    const onRejected = (error: unknown) => {
        console.error(error);
        res.statusCode = 500;
        res.send({ allowed: false, error });
    };

    const onLoad = (result: AllowedFlag | null) => {
        res.statusCode = 200;
        if(result) {
            res.send(result);
        } else {
            res.send({ allowed: false });
        }
    };

    checkRegistrationFlag()
        .then(onLoad)
        .catch(onRejected);
}
