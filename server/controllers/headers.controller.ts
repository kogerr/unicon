import { Request, Response } from 'express';

export function returnHeaders(req: Request, res: Response): void {
    res.statusCode = 200;
    res.send(JSON.stringify(req.headers));
}
