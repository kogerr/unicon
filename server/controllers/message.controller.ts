import { Request, Response } from 'express';
import { DeleteWriteOpResultObject, InsertOneWriteOpResult } from 'mongodb';
import { deleteOneMessage, findMessages, insertMessage } from '../db/dao/message.dao';
import { Message } from '../models/message.interface';

export function addMessage(req: Request, res: Response): void {
    const onFulfilled = (result: InsertOneWriteOpResult) => {
        if (result.result.ok === 1 && result.insertedCount === 1) {
            res.status(200);
            res.send({ success: true });
        } else {
            res.status(500);
            res.send({ success: false });
        }
    };

    const onRejected = (reason: unknown) => {
        res.status(500);
        res.send({ success: false, error: reason });
    };

    insertMessage(req.body).then(onFulfilled, onRejected);
}

export function getMessages(req: Request, res: Response): void {
    const onFulfilled = (result: Message[]) => {
        const messages = result.sort((a, b) => b.date - a.date);
        res.status(200);
        res.send({ messages });
    };

    const onRejected = (reason: unknown) => {
        res.status(500);
        res.send({ error: reason });
    };

    findMessages().then(onFulfilled, onRejected);
}

export function deleteMessage(req: Request, res: Response): void {
    const onFulfilled = (result: DeleteWriteOpResultObject) => {
        if (result.result.ok === 1 && result.deletedCount === 1) {
            res.status(200);
            res.send({ success: true });
        } else {
            res.status(500);
            res.send({ success: false, error: result });
        }
    };

    const onRejected = (reason: unknown) => {
        res.status(500);
        res.send({ success: false, error: reason });
    };

    deleteOneMessage(req.params.id).then(onFulfilled, onRejected);
}
